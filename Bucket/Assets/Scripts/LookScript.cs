﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookScript : MonoBehaviour
{
    public static LookScript instance;
    public GameObject cursor;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime * 60;
        transform.Rotate(new Vector3(0, -Input.GetAxis("L_XAxis_1") * dt, 0));
        cursor.transform.position = new Vector3(
            cursor.transform.position.x + (Input.GetAxis("R_XAxis_1") * dt * 4),
            cursor.transform.position.y - (Input.GetAxis("R_YAxis_1") * dt * 4),
            cursor.transform.position.z);
    }
}
