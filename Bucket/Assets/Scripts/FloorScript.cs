﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorScript : MonoBehaviour
{
    public float explosionSize = 10.0f;
    public float explosionStrength = 100.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("GameBall"))
        {
            foreach (var index in Physics.OverlapSphere(transform.position, explosionSize))
            {
                if (!index.CompareTag("GameBall"))
                {
                    if (index.attachedRigidbody != default)
                    {
                        index.attachedRigidbody.AddExplosionForce(explosionStrength, transform.position, explosionSize, 10.0f);
                    }
                }
            }
            GetComponent<ParticleSystem>().Play();
            StartCoroutine(GameManager.instance.DelayedRestart());
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("GameBall"))
        {
            foreach (var index in Physics.OverlapSphere(transform.position, explosionSize))
            {
                if (!index.CompareTag("GameBall"))
                {
                    if (index.attachedRigidbody != default)
                    {
                        index.attachedRigidbody.AddExplosionForce(explosionStrength, transform.position, explosionSize, 10.0f);
                    }
                }
            }
            GetComponent<ParticleSystem>().Play();
            StartCoroutine(GameManager.instance.DelayedRestart());
        }
    }
}
