﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] float restartTime = 2.0f;
    [SerializeField] float explosionSize = 10.0f;
    [SerializeField] float explosionStrength = 100.0f;
    [SerializeField] float upwardsModifier = 1.0f;
    Renderer rend;
    Color defaultColor;// = Color.black;
    [SerializeField] Color targetColor;// = Color.red;
    //Color currentColor;
    float elapsedTime = 0;
    bool idleMode = false;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        defaultColor = rend.material.GetColor("_BaseColor");
        if (SceneManager.GetActiveScene().name == "Idle")
        {
            idleMode = true;
            rend.enabled = false;
        }
    }

    public IEnumerator DelayedRestart()
    {
        Debug.Log("DelayedRestart() called.");
        yield return new WaitForSeconds(restartTime);
        if (SceneManager.GetActiveScene().name == "Idle")
        {
            SceneManager.LoadScene("Idle", LoadSceneMode.Single);
        }
        else
        {
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!idleMode)
        {
            transform.localScale = Vector3.one * explosionSize;
            RaycastHit hit;
            float dt = Time.deltaTime;

            if (Physics.Raycast(Camera.main.ScreenPointToRay(LookScript.instance.cursor.transform.position), out hit))
            {
                transform.position = new Vector3(
                    Mathf.Lerp(transform.position.x, hit.point.x, dt * 10.0f),
                    Mathf.Lerp(transform.position.y, hit.point.y, dt * 10.0f),
                    Mathf.Lerp(transform.position.z, hit.point.z, dt * 10.0f));
            }
            if (Input.GetButton("RB_1"))
            {
                elapsedTime += dt;
                rend.material.SetColor("_BaseColor", lerpColor(defaultColor, targetColor, elapsedTime));
            }
            if (Input.GetButtonUp("RB_1"))
            {
                elapsedTime = Mathf.Min(elapsedTime, 1.0f);
                foreach (var index in Physics.OverlapSphere(hit.point, explosionSize))
                {
                    if (index.attachedRigidbody != default && !index.CompareTag("GameBall"))
                    {
                        index.attachedRigidbody.AddExplosionForce(explosionStrength + (explosionStrength * elapsedTime * 10), hit.point, explosionSize, upwardsModifier);
                    }
                }
                //Shader.PropertyToID("");
                rend.material.SetColor("_BaseColor", defaultColor);
                elapsedTime = 0;
            }
        }
        if (Input.GetButtonDown("Start_1"))
        {
            StartCoroutine(DelayedRestart());
        }
        if (Input.GetButtonDown("Back_1"))
        {
            if (SceneManager.GetActiveScene().name == "Idle")
            {
                SceneManager.LoadScene("Main", LoadSceneMode.Single);
            }
            else
            {
                SceneManager.LoadScene("Idle", LoadSceneMode.Single);
            }
        }
    }

    Color lerpColor(Color input, Color target, float dt)
    {
        //dt *= 2;
        Color returnMe = new Color(
            Mathf.Lerp(input.r, target.r, dt),
            Mathf.Lerp(input.g, target.g, dt),
            Mathf.Lerp(input.b, target.b, dt),
            Mathf.Lerp(input.a, target.a, dt));
        return returnMe;
    }
}
