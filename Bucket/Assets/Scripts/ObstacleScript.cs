﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleScript : MonoBehaviour
{
    //[SerializeField] Vector3 spawnOrigin = Vector3.zero;
    List<Vector3> spawnPoints = new List<Vector3>();
    [SerializeField] GameObject obstacle;

    // Start is called before the first frame update
    void Start()
    {        
        for (int y = 0; y < 5; y++)
        {
            for (int x = 0; x < 5; x++)
            {
                for (int z = 0; z < 5; z++)
                {
                    spawnPoints.Add(new Vector3(x, y, z) + transform.position);
                    //Instantiate(obstacle, new Vector3(x, y - 0.5f, z), Quaternion.identity, gameObject.transform);
                }
            }
        }
        foreach(var sp in spawnPoints)
        {
            Instantiate(obstacle, sp, Quaternion.identity, gameObject.transform);
        }
        //for (int i = 0; i < 5; i++)
        //{
        //    int randomValue = UnityEngine.Random.Range(0, spawnPoints.Count);
        //    Merge(spawnPoints[randomValue], spawnPoints[randomValue + 1]);
        //}
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Merge(GameObject a, GameObject b)
    {
        a.GetComponent<Rigidbody>().isKinematic = true;
        b.GetComponent<Rigidbody>().isKinematic = true;
        GameObject temp = new GameObject();
        a.transform.parent = temp.transform;
        b.transform.parent = temp.transform;
        temp.AddComponent<Rigidbody>();
    }
}
